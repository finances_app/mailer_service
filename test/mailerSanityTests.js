const assert = require('assert');
const _ = require('lodash');
const superagent = require('superagent');
const quotedPrintable = require('quoted-printable');

describe('Testing the new device notification route', function () {
    it('Should return some html containing the input strings', async function () {
        const errorNotificationConfig = {
            from: 'financesapp.notifications@gmail.com',
            to: 'hugo.truchonti@gmail.com',
            apiAddress: 'http://localhost',
            isDesktop: true,
            device: 'Android - some-build some-webview',
            location: 'Matane, QC, Canada',
            authorizeToken: 'some-uuid',
        };

        // Should not throw exceptions
        let emailId;
        try {
            let sendMailRes = await superagent.post(`http://mailer/send/new_device`).send(errorNotificationConfig);
            emailId = _.chain(sendMailRes).get('body.response', ' ').split(' ').last().value();
        } catch (err) {
            assert.fail(
                'mailer service failed: ' +
                    JSON.stringify(
                        {
                            code: err.code,
                            syscall: err.syscall,
                            stack: err.stack,
                            message: err.message,
                            hostname: err.hostname,
                            response: err.response,
                        },
                        null,
                        2
                    )
            );
        }

        assert.ok(emailId, 'no email ID found in the response.');

        // Check if the email was received in mailhogger
        const response = await superagent.get('http://mailhog:8025/api/v2/messages');
        let body = JSON.parse(response.text);
        assert.ok(body.items, 'mailhog response is missing properties :' + JSON.stringify(body));

        const sentEmail = body.items.find((_item) => _item.ID === emailId);
        assert.ok(sentEmail, `No email matching id ${emailId} found in mailhogger.`);

        // parsing the email body to remove some encoded HTML chars
        const parsedBody = quotedPrintable.decode(sentEmail.Content.Body);

        assert.ok(
            parsedBody.includes(errorNotificationConfig.device),
            'Message body does not include the configured device name.'
        );
        assert.ok(
            parsedBody.includes(errorNotificationConfig.location),
            'Message body does not include the configured location.'
        );
        assert.ok(
            parsedBody.includes(errorNotificationConfig.authorizeToken),
            'Message body does not include the configured auth token.'
        );
    });
});
