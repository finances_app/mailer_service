/**
 * Created by hugo on 2018-12-30
 */
// const nodemailer = require('nodemailer');
const fs = require('fs');
const util = require('util');
const path = require('path');
const mjml = require('mjml');
const _ = require('lodash');
const dayjs = require('dayjs');
const sgMail = require('@sendgrid/mail');

const mailerSecrets = JSON.parse(fs.readFileSync(process.env.MAILER_CONFIG_PATH));
sgMail.setApiKey(mailerSecrets.sendGridApiKey);

const fs_readFile = util.promisify(fs.readFile);

class mailService {
    /**
     * Send an email with custom content.
     * @param {object} options
     * @param {string} options.from
     * @param {string} options.to
     * @param {string} options.subject
     * @param {string} options.text
     * @param {string} options.html
     */
    static async sendEmail(options) {
        return await sgMail.send(options);
    }

    /**
     *
     * @param {object} options
     * @param {string} options.from
     * @param {string} options.to
     * @param {string} options.apiAddress
     * @param {string} options.moduleName
     * @param {string} options.errorMessage
     * @param {string} options.errorStackTrace
     */
    static async sendErrorEmail(options) {
        let templatePath = path.join(__dirname, 'templates', 'error_notification.mjml');
        let template = await fs_readFile(templatePath, { encoding: 'utf-8' });

        let tplFn = _.template(template);
        let res = mjml(
            tplFn({
                serverAddress: options.apiAddress,
                moduleName: options.moduleName,
                errorMessage: options.errorMessage,
                stackTrace: options.errorStackTrace.replace(/[\n]/gi, '<br/><br/>'),
            }),
            { minify: true }
        );

        return await sgMail.send(
            _.extend(options, {
                subject: `[Finances app] Rapport d'erreur - ${options.moduleName}`,
                html: res.html,
            })
        );
    }

    /**
     * @depreciated
     * @param {object} options
     * @param {string} options.from
     * @param {string} options.to
     * @param {string} options.apiAddress
     * @param {boolean} [options.isDesktop]
     * @param {string} options.device
     * @param {string} options.location
     * @param {string} options.authorizeToken
     */
    static async sendNewDeviceActivity(options) {
        let templatePath = path.join(__dirname, 'templates', 'new_device.mjml');
        let template = await fs_readFile(templatePath, { encoding: 'utf-8' });

        const tplFn = _.template(template);
        let res = mjml(
            tplFn({
                serverAddress: options.apiAddress,
                activityDate: dayjs().format('YYYY-MM-DD'),
                activityTime: dayjs().format('HH:mm Z'),
                authorizeAccountUrl: `/authorize?token=${options.authorizeToken}`,
                deviceIcon: options.isDesktop ? 'desktop' : 'smartphone',
                deviceName: options.device,
                deviceLocation: options.location,
            }),
            { minify: true }
        );

        return await sgMail.send(
            _.extend(options, {
                subject: `[Finances app] Un nouvel appareil a été utilisé pour se connecté à vôtre compte`,
                html: res.html,
            })
        );
    }

    /**
     * @param {object} options
     * @param {string} options.from
     * @param {string} options.to
     * @param {string} options.apiAddress
     * @param {string} options.secretQuestion
     */
    static async sendSecurityQuestion(options) {
        let templatePath = path.join(__dirname, 'templates', 'secret_question.mjml');
        let template = await fs_readFile(templatePath, { encoding: 'utf-8' });

        let tplFn = _.template(template);
        let res = mjml(
            tplFn({
                serverAddress: options.apiAddress,
                secretQuestion: options.secretQuestion,
            }),
            { minify: true }
        );

        return await sgMail.send(
            _.extend(options, {
                subject: `[Finances app] Erreur du crawler`,
                html: res.html,
            })
        );
    }
}
module.exports = mailService;
