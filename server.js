const express = require('express');
const bodyParser = require('body-parser');

const mailService = require('./mailService');

const app = express();

app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
app.use(bodyParser.json({ limit: '5mb' }));

app.post('/send/error', async (req, res) => {
    const requiredFields = ['from', 'to', 'apiAddress', 'moduleName', 'errorMessage'];

    /**
     * @property {string} from
     * @property {string} to
     * @property {string} apiAddress
     * @property {string} moduleName
     * @property {string} errorMessage
     * @property {string} errorStackTrace
     */
    let config = req.body;

    if (requiredFields.some((_field) => !config[_field])) {
        return res.status(400).send({
            message: `Invalid configuration. Fields [${requiredFields.join(', ')}] are required.`,
        });
    }

    try {
        let response = await mailService.sendErrorEmail(config);
        res.send(response);
    } catch (err) {
        console.error(err);
        res.status(500).send({
            message: err.message,
            stack: err.stack,
        });
    }
});

/**
 * @depreciated
 */
app.post('/send/new_device', async (req, res) => {
    const requiredFields = ['from', 'to', 'apiAddress', 'authorizeToken'];

    /**
     * @property {string} from
     * @property {string} to
     * @property {string} apiAddress
     * @property {boolean} [isDesktop]
     * @porperty {string} device
     * @property {string} location
     * @property {string} authorizeToken
     */
    const config = req.body;

    if (requiredFields.some((_field) => !config[_field])) {
        return res.status(400).send({
            message: `Invalid configuration. Fields [${requiredFields.join(', ')}] are required.`,
        });
    }

    try {
        let response = await mailService.sendNewDeviceActivity(config);
        res.send(response);
    } catch (err) {
        console.error(err);
        res.status(500).send({
            message: err.message,
            stack: err.stack,
        });
    }
});

app.post('/send/security_question', async (req, res) => {
    const requiredFields = ['from', 'to', 'apiAddress', 'secretQuestion'];

    /**
     * @property {string} from
     * @property {string} to
     * @property {string} apiAddress
     * @property {string} secretQuestion
     */
    const config = req.body;

    if (requiredFields.some((_field) => !config[_field])) {
        return res.status(400).send({
            message: `Invalid configuration. Fields [${requiredFields.join(', ')}] are required.`,
        });
    }

    try {
        let response = await mailService.sendSecurityQuestion(config);
        res.send(response);
    } catch (err) {
        console.error(err);
        res.status(500).send({
            message: err.message,
            stack: err.stack,
        });
    }
});

app.listen(80);
